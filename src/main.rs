use crate::hittable::*;
use crate::ray::Ray;
use crate::sphere::Sphere;
use crate::types::*;
use anyhow::Result;
use std::env;
use std::fs::File;
use std::io::Write;
use std::process::Command;
use std::rc::Rc;

mod hittable;
mod ray;
mod sphere;
mod types;

fn main() -> Result<()> {
    let mut dir = env::temp_dir();
    dir.push("out.ppm");
    let file = File::create(&dir)?;
    draw(&file)?;

    println!("{:?}", dir);

    Command::new("convert")
        .arg(dir)
        .arg("out.png")
        .spawn()?
        .wait()?;

    Ok(())
}

fn draw(mut out: impl std::io::Write + Copy) -> Result<()> {
    // Image
    let aspect_ratio = 16.0 / 9.0;
    let image_width = 400;
    let image_height = (image_width as f64 / aspect_ratio) as i32;

    // World
    let mut world = HittableList::new();
    world.add(Rc::new(Box::new(Sphere::new(
        Point3::new(0., 0., -1.),
        0.5,
    ))));
    world.add(Rc::new(Box::new(Sphere::new(
        Point3::new(0., -100.5, -1.),
        100.,
    ))));

    // Camera
    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_length = 1.0;

    let origin = Vec3::new(0., 0., 0.);
    let horizontal = Vec3::new(viewport_width, 0., 0.);
    let vertical = Vec3::new(0., viewport_height, 0.);
    let lower_left_corner =
        origin - horizontal / 2. - vertical / 2. - Vec3::new(0., 0., focal_length);

    out.write(format!("P3\n{} {}\n255\n", image_width, image_height).as_bytes())?;

    for j in (0..image_height).rev() {
        for i in 0..image_width {
            let u = (i as f64) / ((image_width - 1) as f64);
            let v = (j as f64) / ((image_height - 1) as f64);
            let r = Ray::new(
                origin,
                lower_left_corner + u * horizontal + v * vertical - origin,
            );
            let pixel_color = ray_color(&r, &world);
            write_color(out, pixel_color)?;
        }
    }

    Ok(())
}

fn write_color<T: Write>(mut out: T, color: Color) -> Result<usize> {
    let space: &[u8] = b" ";
    let newline: &[u8] = b"\n";
    let mut written: usize = 0;
    written += out.write(format!("{:.0}", 255.999 * color.x).as_bytes())?;
    written += out.write(space)?;
    written += out.write(format!("{:.0}", 255.999 * color.y).as_bytes())?;
    written += out.write(space)?;
    written += out.write(format!("{:.0}", 255.999 * color.z).as_bytes())?;
    written += out.write(newline)?;
    Ok(written)
}

fn ray_color(r: &Ray, world: &HittableList) -> Color {
    if let Some(hit) = world.hit(r, 0., f64::INFINITY) {
        return 0.5 * (hit.normal + color(1., 1., 1.));
    }
    let unit_direction = unit_vector(r.direction());
    let t = 0.5 * (unit_direction.y + 1.0);
    return color(1.0, 1.0, 1.0).lerp(color(0.5, 0.7, 1.0), t);
}

fn unit_vector(vec: Vec3) -> Vec3 {
    vec / vec.length()
}

fn color(r: f64, g: f64, b: f64) -> Vec3 {
    Vec3::new(r, g, b)
}
