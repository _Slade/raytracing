use crate::types::*;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    orig: Point3,
    dir: Vec3,
}

impl Ray {
    pub fn new(orig: Point3, dir: Vec3) -> Self {
        Self { orig, dir }
    }

    pub fn at(self, t: f64) -> Point3 {
        self.orig + t * self.dir
    }

    pub fn direction(self) -> Vec3 {
        self.dir
    }

    pub fn origin(self) -> Point3 {
        self.orig
    }
}
