use glam::f64::DVec3;

pub type Vec3 = DVec3;
pub type Point3 = DVec3;
pub type Color = DVec3;
